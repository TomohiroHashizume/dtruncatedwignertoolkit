import os,sys
import numpy as np
import scipy as sp
import networkx as nx

from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis

import matplotlib.pyplot as plt


L=15
dt=0.01
tf=5
Jzz=1.
hx=0.

alpha=3.

spin=1/2.
nt=1000

pauli=False
nsw=int(tf/dt)
fname_ED = './testdata/ED_out.out'
if not os.path.exists('./testdata/') :
   os.makedirs('./tesdata/')

if not os.path.exists(fname_ED) :

   G=nx.erdos_renyi_graph(L, p=1.0)
   edges=G.edges

   basis = spin_basis_1d(L,pauli=pauli)

   J_zz = [[Jzz*(np.min([np.abs(i-j),L-np.abs(i-j)])**(-alpha)),i,j] for i,j in edges]
   h_x=[[hx,i] for i in range(L)]

   static = [["zz",J_zz],["x",h_x]]
   dynamic=[]

   m1idtHamil = -1j*dt*hamiltonian(static,dynamic,basis=basis,dtype=np.complex128).tocsc()
   psi0=1.
   for i in range(L) :
      psi0=np.kron([1.0,1.0]/np.sqrt(2.0),psi0)
   psi0=np.array(psi0,dtype=np.complex128)

   h_z=[[1,i] for i in range(L)]
   static = [["x",h_z]]
   SzOp = hamiltonian(static,dynamic,basis=basis,dtype=np.complex128)

   psi0=np.array(psi0,dtype=np.complex128)
   psi0_c=np.conj(psi0)
   psi_t=psi0.copy()

   ts=[]
   Szs=[]
   for i in range(nsw) :
      t=np.real(i*dt)
      print(t)


      psi_t=sp.sparse.linalg.expm_multiply(m1idtHamil,psi_t)

      Sz=np.real(SzOp.expt_value(psi_t))

      ts+=[t]
      Szs+=[Sz/L]
   np.savetxt(fname_ED,[ts,Szs])

if os.path.exists(fname_ED) :
   dat=np.loadtxt(fname_ED)
   print(dat.shape)
   plt.plot(dat[0,:],dat[1,:])

print('DTWA')
#from scipy import integrate #import DOP853
sys.path.append('../')
import gdtwa_spin as gs


print('L=',L)
print('hx=',hx)

fname='testdata/dtwa_out.out'
if not os.path.exists(fname) :
   verbose=False
   ortho_mat_type='Gell_Mann'
   testvar=gs.gdtwa_spin(spin,is_boson=False,verbose=verbose,ortho_mat_type=ortho_mat_type)

   hx_=(1.+pauli)*hx

   state_in=[]
   count = 0
   for ind in range(L) :
      currstate=np.array([1,1],dtype=np.complex128)/np.sqrt(2)
      state_in.append(currstate.copy())

   state_in=np.array(state_in)

   h_x=hx_*np.ones(L,dtype=np.complex128)
   h_y=np.zeros(L,dtype=np.complex128)
   h_z=np.zeros(L,dtype=np.complex128)
   h_i=np.zeros(L,dtype=np.complex128)

   J_x = np.zeros((L,L),dtype=np.complex128)
   J_y = np.zeros((L,L),dtype=np.complex128)

   G = nx.erdos_renyi_graph(L,p=1.0)
   J_z=np.zeros((L,L),dtype=np.complex128)

   Jzz_=Jzz
   Jzz_=((1.+pauli)**2.)*Jzz_
   numedges=G.number_of_edges() 

   for edge in G.edges :
      i=edge[0]
      j=edge[1]
      J_z[edge[0],edge[1]] = Jzz_*(Jzz*(np.min([np.abs(i-j),L-np.abs(i-j)])**(-alpha)))
      J_z[edge[1],edge[0]] = J_z[edge[0],edge[1]]

   testvar.set_initial(state_in,nt,seed=770072400)
   testvar.set_Hamil(h_x,h_y,h_z,h_i,J_x,J_y,J_z)

   Op=[testvar.Orthomat_coeffs['Sx']]*testvar.N
   testvar.evolve(5.,int(5/0.01),Op=Op,fname=fname)

if os.path.exists(fname) :
   dat=np.loadtxt(fname,delimiter=',')
   print(dat.shape)
   plt.plot(np.arange(nsw)*dt,(1+pauli)*np.sum(dat,axis=1)/L,'ko')
plt.show()
