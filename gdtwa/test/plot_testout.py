import numpy as np
import matplotlib.pyplot as plt

import sys,os
sys.path.append('../')
import gdtwa_spin as gs

nbosons = 4
spin=nbosons/2.

Lx=11
Ly=Lx
L=Lx*Ly

outdir='../testdata/'
fname='./testdata/out.out'

nt=100
if (os.path.exists(fname) and os.path.exists(fname+'_sq')) :
   M=np.genfromtxt(fname,delimiter=',')
   Merr=np.sqrt(np.genfromtxt(fname+'_sq',delimiter=',')-(M)**2.)/np.sqrt(nt-1)

   plt.matshow(M[0].reshape(Lx,Ly))
   plt.title('initial')
   plt.colorbar()

   plt.matshow(M[-1].reshape(Lx,Ly))
   plt.title('final')
   plt.colorbar()
   plt.show()

   plt.errorbar(np.arange(Lx),np.sum(M[0].reshape(Lx,Ly),axis=1) ,np.sqrt(np.sum(Merr[0].reshape(Lx,Ly)**2. ,axis=1) ))
   plt.errorbar(np.arange(Lx),np.sum(M[-1].reshape(Lx,Ly),axis=1),np.sqrt(np.sum(Merr[-1].reshape(Lx,Ly)**2.,axis=1) ))
   plt.show()
