import sys,os
import numpy as np
from scipy import integrate #import DOP853
from datetime import datetime 
print('Program started at:',datetime.now())

sys.path.append('../')
import gdtwa_boson as gb

nbosons = 4
verbose=False
verbose=True
ortho_mat_type='Gell_Mann'
testvar=gb.gdtwa_boson(nbosons,verbose=verbose,ortho_mat_type=ortho_mat_type)

# gdTWA for bose Hubbard model on square lattice with random disorder potential

U=2.0
J=1.0
Wq=1.2

print('U={0:.2f}, J={1:.2f}, Wq={2:.2f}'.format(U,J,Wq))

Lx=11
numexcitex=5

numexcitey=numexcitex 
Ly=Lx

L=Lx*Ly
nt=100

state_in=[]
count = 0
for ind in range(L) :
   m = ind % Lx
   n = ind // Ly

   in_x=( (m >= (Lx/2 - numexcitex/2))  and ( m < (Lx/2 + numexcitex/2)) )
   in_y=( (n >= (Ly/2 - numexcitex/2))  and ( n < (Ly/2 + numexcitex/2)) )

   currstate=np.zeros(nbosons+1)
   if in_x and in_y :
      currstate[-2]=1
   else :
      currstate[-1]=1
   state_in.append(currstate)

state_in=np.array(state_in)
testvar.set_initial(state_in,nt)

h_n=Wq*np.array(np.random.uniform(-1,1,L),dtype=np.complex128)/2.
h_n = h_n - ( U / 2.) *np.ones(L,dtype=np.complex128)

J_t=np.zeros((L,L),dtype=np.complex128)
for indi in range(L) :
   m1 = indi % Lx
   n1 = indi // Ly
   for indj in range(L) :
      m2 = indj %  Lx
      n2 = indj // Ly

      if (abs(m2-m1)==1) and (n2==n1) :
         J_t[indi,indj] = 1/2.
      elif (m2==m1) and (abs(n2-n1)==1) :
         J_t[indi,indj] = 1/2.

J_t *= -J

U_int = (U/2.)*np.ones((L,1),dtype=np.complex128) # onsite

testvar.set_Hamil(h_n,J_t,U_int)
testvar.evolve(20.,int(20/0.1),outdir='./testdata/') #,method="LSODA" ,rtol=1e-14,atol=1e-14)
