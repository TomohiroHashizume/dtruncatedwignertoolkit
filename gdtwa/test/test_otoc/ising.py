import os,sys
import numpy as np
import scipy as sp
import networkx as nx

from quspin.operators import hamiltonian # Hamiltonians and operators
from quspin.basis import spin_basis_1d # Hilbert space spin basis

import matplotlib.pyplot as plt


L=10
dt=0.01
tf=5
Jzz=1.
hx=1.

alpha=3.

spin=1/2.
nt=100

G=nx.erdos_renyi_graph(L, p=1.0,seed=19940527)
edges=G.edges

pauli=False
nsw=int(tf/dt)
fname_ED = './testdata/ED_out.out'
if (not os.path.exists('./testdata/')) :
   os.makedirs('./testdata/')


if not os.path.exists(fname_ED) :


   basis = spin_basis_1d(L,pauli=pauli)

   J_zz = [[Jzz*(np.min([np.abs(i-j),L-np.abs(i-j)])**(-alpha)),i,j] for i,j in edges]
   h_x=[[hx,i] for i in range(L)]

   static = [["zz",J_zz],["x",h_x]]
   dynamic=[]

   HH = hamiltonian(static,dynamic,basis=basis,dtype=np.complex128).tocsc()

   psi0=1.
   for i in range(L) :
      psi0=np.kron([1.,0+1j*0],psi0)
   psi0=np.array(psi0,dtype=np.complex128)

   ZZ=[[1.,0],[0,-1.+0*1j]]
   II=[[1.,0],[0,1.+0*1j]]
   Sz0=ZZ.copy()
   for i in range(1,L) :
      Sz0=np.kron(Sz0,II)

   Sz1=1.
   for i in range(L) :
      opnow=II.copy()
      if i==1 :
         opnow=ZZ.copy()
      Sz1=np.kron(Sz1,opnow)

   ts=[]
   OTOCs=[]
   for i in range(nsw) :
      tnow=np.real((i+1)*dt)
      m1idtHamil = -1j*tnow*HH

      psi_t1=Sz0@psi0.copy()

      psi_t1=sp.sparse.linalg.expm_multiply(m1idtHamil,psi_t1)
      psi_t1=Sz1@psi_t1
      psi_t1=sp.sparse.linalg.expm_multiply(-m1idtHamil,psi_t1)


      psi_t2=psi0.copy()
      psi_t2=sp.sparse.linalg.expm_multiply( m1idtHamil,psi_t2)
      psi_t2=Sz1@psi_t2
      psi_t2=sp.sparse.linalg.expm_multiply(-m1idtHamil,psi_t2)
      psi_t2=Sz0@psi_t2

      OTOC=2-2*np.real(np.conj(psi_t2)@psi_t1)
      print(f't={tnow:.3f}',OTOC)
      ts+=[tnow]
      OTOCs+=[OTOC]
   np.savetxt(fname_ED,[ts,OTOCs])

if os.path.exists(fname_ED) :
   dat=np.loadtxt(fname_ED)
   print(dat.shape)
   plt.plot(dat[0,:],dat[1,:])

print('DTWA')
#from scipy import integrate #import DOP853
sys.path.append('../../')
import gdtwa_spin as gs


print('L=',L)
print('hx=',hx)

fname='testdata/dtwa_out.out'
verbose=False
ortho_mat_type='Gell_Mann'
testvar=gs.gdtwa_spin(spin,is_boson=False,verbose=verbose,ortho_mat_type=ortho_mat_type)

hx_=(1.+pauli)*hx

state_in=[]
count = 0
for ind in range(L) :
   currstate=np.array([1,0],dtype=np.complex128)
   state_in.append(currstate.copy())

state_in=np.array(state_in)

h_x=hx_*np.ones(L,dtype=np.complex128)
h_y=np.zeros(L,dtype=np.complex128)
h_z=np.zeros(L,dtype=np.complex128)
h_i=np.zeros(L,dtype=np.complex128)

J_x = np.zeros((L,L),dtype=np.complex128)
J_y = np.zeros((L,L),dtype=np.complex128)

J_z=np.zeros((L,L),dtype=np.complex128)

Jzz_=Jzz
Jzz_=((1.+pauli)**2.)*Jzz_
numedges=G.number_of_edges() 

for edge in G.edges :
   i=edge[0]
   j=edge[1]
   J_z[edge[0],edge[1]] = Jzz_*((np.min([np.abs(i-j),L-np.abs(i-j)])**(-alpha)))
   J_z[edge[1],edge[0]] = J_z[edge[0],edge[1]]

testvar.set_initial(state_in,nt,seed=770072400)
testvar.set_Hamil(h_x,h_y,h_z,h_i,J_x,J_y,J_z)

Op1=[2*testvar.Orthomat_coeffs['Sz']]
sites1=[0]
LogOp2=[-np.pi*2*testvar.Orthomat_coeffs['Sz']/2.+np.pi*testvar.Orthomat_coeffs['I']/2.]
sites2=[1]
testvar.evaluate_otoc(5.,dt,Op1=Op1,sites1=sites1,LogOp2=LogOp2,sites2=sites2,fname=fname,update=True)

if os.path.exists(fname) :
   dat=np.loadtxt(fname)
   plt.plot(dat[:,0],2-2*dat[:,1],'ko')
plt.show()
