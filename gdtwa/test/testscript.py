import sys,os
import numpy as np
from scipy import integrate #import DOP853
from datetime import datetime 

sys.path.append('../')
import gdtwa_spin as gs

print('Program started at:',datetime.now())

nbosons = 4
spin=nbosons/2.

# choose verbose option
#verbose=False
verbose=True

# orthogonal matrices to use
#ortho_mat_type='Clock_and_Shift'
ortho_mat_type='Gell_Mann'

testvar=gs.gdtwa_spin(spin,is_boson=True,verbose=verbose,ortho_mat_type=ortho_mat_type)

# gdTWA for XXZ model on square lattice with random disorder potential

U=2.0
J=1.0
Wq=1.2

print('U={0:.2f}, J={1:.2f}, Wq={2:.2f}'.format(U,J,Wq))

Lx=11
numexcitex=5

numexcitey=numexcitex 
Ly=Lx

L=Lx*Ly
nt=100

state_in=[]
count = 0
for ind in range(L) :
   m = ind % Lx
   n = ind // Ly

   in_x=( (m >= (Lx/2 - numexcitex/2))  and ( m < (Lx/2 + numexcitex/2)) )
   in_y=( (n >= (Ly/2 - numexcitey/2))  and ( n < (Ly/2 + numexcitey/2)) )

   currstate=np.zeros(nbosons+1)
   if in_x and in_y :
      currstate[-2]=1
   else :
      currstate[-1]=1
   state_in.append(currstate)

state_in=np.array(state_in)
testvar.set_initial(state_in,nt)

h_x=np.zeros(L,dtype=np.complex128)
h_y=np.zeros(L,dtype=np.complex128)
h_z=Wq*np.array(np.random.uniform(-1,1,L),dtype=np.complex128)/2.
h_i=h_z.copy()*spin

h_z = h_z + U*(2.*spin-1.)*np.ones(L,dtype=np.complex128)
J_x=np.zeros((L,L),dtype=np.complex128)
for indi in range(L) :
   m1 = indi % Lx
   n1 = indi // Ly
   for indj in range(L) :
      m2 = indj %  Lx
      n2 = indj // Ly

      if (abs(m2-m1)==1) and (n2==n1) :
         J_x[indi,indj] = 1
      elif (m2==m1) and (abs(n2-n1)==1) :
         J_x[indi,indj] = 1

J_x *= -J
J_y = J_x.copy()
J_z = U*(np.zeros(L,dtype=np.complex128) + np.diag(np.ones(L,dtype=np.complex128))) /2. # onsite

Op=[testvar.Orthomat_coeffs['Sz']+spin*testvar.Orthomat_coeffs['I']]*testvar.N
testvar.set_Hamil(h_x,h_y,h_z,h_i,J_x,J_y,J_z)
if not os.path.exists('./testdata') :
   os.makedirs('./testdata')

testvar.evolve(20.,int(20/0.1),Op=Op,fname='./testdata/out.out') #,method="LSODA" ,rtol=1e-14,atol=1e-14)
