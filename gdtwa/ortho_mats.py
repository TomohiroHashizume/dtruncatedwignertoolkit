import sys
from scipy.sparse import csr_matrix as cm
import numpy as np

# Places marked with ** can be more efficient, but remember
# Always optimize after you measure and fond that the place is really
# the bottleneck.

global machine_eps
machine_eps=1e-14

def Gell_Mann_mat(spin) :
   locdim=int(2*spin+1)
   rhoD  =int(locdim**2)

   mats=[] 
   for m in range(0,locdim) : 
      for n in range(m+1,locdim) :
         mat=np.asmatrix(np.zeros((locdim,locdim),dtype=np.complex128))
         if n>m :
            mat[m,n] += np.sqrt(1./2.)
            mat = mat+mat.H
            mats.append(cm(mat))

   for m in range(0,locdim) :
      for n in range(m+1,locdim) :
         mat=np.asmatrix(np.zeros((locdim,locdim),dtype=np.complex128))
         if n > m :
            mat[m,n]=1.
            mat = 1./(np.sqrt(2)*1j)*(mat-mat.H)
            mats.append(cm(mat))
   #** 

   for a in range(0,locdim-1) :
      mat=np.asmatrix(np.zeros((locdim,locdim)),dtype=np.complex128)
      locmat=np.asmatrix(np.zeros((locdim,locdim)),dtype=np.complex128) 
      for n in range(0,a+1) :
         locmat[n,n] = (1/np.sqrt((a+1)*(a+2)))  
         locmat[a+1,a+1] = -((a+1)/np.sqrt((a+1)*(a+2)))

      mat += locmat
      mats.append(cm(mat))

   Lambda_d_eye=np.sqrt(1/locdim)*np.eye(locdim)

   mats.append(cm(Lambda_d_eye))

   if (len(mats) != int((2*spin+1)**2)) :
      print('something went wrong in a creation of GGMs.', len(mats),(2*spin+1)**2)
      sys.exit(0)
   return mats

def Clock_and_Shift_mat(spin) :
   locdim=int(2*spin+1)
   rhoD  =int(locdim**2)

   # https://en.wikipedia.org/wiki/Generalizations_of_Pauli_matrices#A_non-Hermitian_generalization_of_Pauli_matrices
   # I TRUST WIKIPEDIAAAAAAA

   # For more legit source:
   # Ashmeet Singh, Sean M. Carroll
   # : “Modeling Position and Momentum in Finite-Dimensional Hilbert Spaces via Generalized Pauli Operators”, 2018;
   # [http://arxiv.org/abs/1806.10134 arXiv:1806.10134].

   Sigma1=np.asmatrix(np.zeros((locdim,locdim),dtype=np.complex128))
   Sigma1[0,-1]=1.
   for m in range(1,locdim) :
      Sigma1[m,m-1]=1.

   omega=np.exp(2.*np.pi*1j/locdim)
   Sigma3=np.asmatrix(np.zeros((locdim,locdim),dtype=np.complex128))
   for m in range(locdim) :
      Sigma3[m,m]=omega**m

   mats=[] 
   for m in range(locdim) :
      for n in range(locdim) :
         mats.append(cm(np.linalg.matrix_power(Sigma1,m)@np.linalg.matrix_power(Sigma3,n))/np.sqrt(locdim))

   if (len(mats) != int((2*spin+1)**2)) :
      print('something went wrong in a creation of GGMs.', len(mats),(2*spin+1)**2)
      sys.exit(0)
   return mats

def Get_eigs(mats) :
   eigvecs=[]
   eigvals=[]
   for ind in range(len(mats)) :
      [w,v] = np.linalg.eig(mats[ind].todense())
      vdash=[]
      for ind in range(v.shape[1]) :
         vdash.append(v[:,ind])
         if np.abs(w[ind]) < machine_eps :
            w[ind]=0.

      eigvecs.append(vdash)
      eigvals.append(w)

   return eigvecs,eigvals

def check_decomposition(physicalmats,gell_mann_mats,gell_mann_coeffs) :
   thresh=1e-14*gell_mann_mats[0].size

   for key in physicalmats.keys() :
      mat=np.asmatrix(np.zeros((gell_mann_mats[-1].shape[0],gell_mann_mats[-1].shape[1])),dtype=np.complex128)
      for ind in range(0,len(gell_mann_mats)) :
         mat += gell_mann_coeffs[key][ind]*gell_mann_mats[ind]
      absdiff=np.sum(np.abs(physicalmats[key]-mat))

      if (thresh < absdiff) :
         print('decomposition did not go well for ',key,' diff=',absdiff)
   return 0

def decompose(mat,ortho_mats,ortho_mat_type) :
   coeffs=[]
   for om in ortho_mats :
      if ortho_mat_type=='Gell_Mann' :
         coeffnow=np.trace(mat @ om )

      elif ortho_mat_type=='Clock_and_Shift' :
         coeffnow=np.trace(mat @ np.linalg.inv(om) ) /mat.shape[0]

      else :
         print('Option',ortho_mat_type,'is not valid')
         print('Choose from: Gell_Mann [default], Clock_and_Shift')
         sys.exit(0)

      if np.abs(coeffnow) > machine_eps :
         coeffs.append(coeffnow)
      else :
         coeffs.append(0.)
   return np.array(coeffs,dtype=np.complex128)

def decompose_physical_spin(physicalmats,ortho_mats,ortho_mat_type) :
   decomposed_coeffs = {}

   for key in physicalmats.keys() :
      matnow=physicalmats[key]
      decomposed_coeffs[key]=decompose(matnow,ortho_mats,ortho_mat_type)

      if (len(decomposed_coeffs[key]) != len(ortho_mats)) :
         print('something went wrong in a process of decomposition.')
         exit(0)
   
   #debug, can be commented out it does not effect performance.
   check_decomposition(physicalmats,ortho_mats,decomposed_coeffs) 

   return decomposed_coeffs

def decompose_commutator(mat1,gell_mann_mats_dense,ortho_mat_type) :
   mats=list(map(lambda x,y : x @ y - y @ x , [mat1]*len(gell_mann_mats_dense),gell_mann_mats_dense))
   return list(map(decompose,mats,[gell_mann_mats_dense]*len(mats),[ortho_mat_type]*len(mats)))

def decompose_commutator_all(gell_mann_mats_dense,ortho_mat_type) :
   out=np.array(list( map(decompose_commutator,
      gell_mann_mats_dense,[gell_mann_mats_dense]*len(gell_mann_mats_dense), 
      [ortho_mat_type]*len(gell_mann_mats_dense)
      )))
   return out
