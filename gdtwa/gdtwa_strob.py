from scipy.sparse import csr_matrix as cm

import opt_einsum as oe

import scipy.integrate
import numpy as np

import basis_mat as bm
import ortho_mats as om

import gdtwa_spin as gs 

import time 
import sys,os

# Python implementation of Generalized Discrete Truncated Wigner 
# for arbitrary spin magnitude, S.
# based on the following papers, please cite:
# * Schachenmayer, J., Pikovski, A., & Rey, A. M. (2015). Many-Body Quantum Spin Dynamics with Monte Carlo Trajectories on a Discrete Phase Space. 
# In Physical Review X (Vol. 5, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physrevx.5.011022.
# * Zhu, B., Rey, A. M., & Schachenmayer, J. (2019). A generalized phase space approach for solving quantum spin dynamics. 
# In New Journal of Physics (Vol. 21, Issue 8, p. 082001). IOP Publishing. https://doi.org/10.1088/1367-2630/ab354d.

global max_num_traj 
max_num_traj = 10000

def grab_choice(val,prob,numtraj) :
   return np.random.choice(val,size=(numtraj),p=prob) 

def normalize_state(state) :
   for ind in range(state.shape[0]) :
      state[ind,:] = state[ind,:]/np.sqrt(sum(state[ind,:]*np.conj(state[ind,:])))
   return state

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class gdtwa_strob(gs.gdtwa_spin) :
   def __init__(self,spin,is_boson=False,ortho_mat_type='Gell_Mann',verbose=False) :
      super().__init__(spin,is_boson=False,ortho_mat_type='Gell_Mann',verbose=False)

   def set_Hamils(self,h_xs,h_ys,h_zs,h_is,J_xs,J_ys,J_zs):
      self.h_xs=h_xs
      self.h_ys=h_ys
      self.h_zs=h_zs
      self.h_is=h_is

      self.J_xs=J_xs
      self.J_ys=J_ys
      self.J_zs=J_zs

      assert len(h_xs) == len(h_ys)
      assert len(h_xs) == len(h_zs)
      assert len(h_xs) == len(h_is)

      assert len(h_xs) == len(J_xs)
      assert len(h_xs) == len(J_ys)
      assert len(h_xs) == len(J_zs)

      self.__make_Hamils()

   def __make_Hamils(self):
      t = time.time()

      self.Hamil_Us=[]
      self.Hamil_Ws=[]

      for i in range(len(self.h_xs)) :
         Hamil_U_i_mu=np.zeros((self.N,self.locdim*self.locdim),dtype=np.complex128)
         for site in range(self.N) :
            Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sx'])*self.h_xs[i][site]
            Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sy'])*self.h_ys[i][site]
            Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sz'])*self.h_zs[i][site]
            Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['I'])*self.h_is[i][site]
         self.Hamil_Us +=[Hamil_U_i_mu.copy()]

         J=[self.J_xs[i],self.J_ys[i],self.J_zs[i]]
         coeffs=[
               self.Orthomat_coeffs['Sx'],
               self.Orthomat_coeffs['Sy'],
               self.Orthomat_coeffs['Sz']
               ]
         W_i_j_mu_nu=oe.contract('sij,sm,sn->ijmn',J,coeffs,coeffs)
         self.Hamil_Ws +=[W_i_j_mu_nu.copy()]

         if (self.verbose) :
            print('shape of single site coefficients: ', Hamil_U_i_mu.shape,flush=True)

         if (self.verbose) :
            print('shape of two site coefficients: ', W_i_j_mu_nu.shape,flush=True)

         elapsed = time.time() - t
         print('finished creating the Hamiltonian',i,': ', elapsed, ' seconds (', elapsed/60., ' minutes)')
      print('finished creating the Hamiltonian: ', elapsed, ' seconds (', elapsed/60., ' minutes)')

   def evolve_strob(self,Tf,Op=None,opsites=None,method='RK45',rtol=1e-3,atol=1e-6,fname='dat.out',update=False,flush=False) :
      if Op is None :
         self.Op=[Orthomat_coeffs['Sz']]*self.N
      else :
         self.Op=Op
      if opsites is None :
         self.opsites = np.arange(self.N,dtype=int)
      else :
         self.opsites=opsites

      self.Tf=Tf # number of layers
      self.ts=np.arange(Tf+1)
      self.statenow=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
      self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
      self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)
      self.fname=fname
      self.fname_var=fname+'_sq'

      self.flush=flush
      if update :
         self.fout=open(self.fname,'w')
         self.fvar=open(self.fname_var,'w')

      self.out_exps=[]
      self.out_exp_vars=[]

      self.__generate_optim_paths(self.statenow.copy())

      assert len(self.Hamil_Us) == len(self.Hamil_Ws) 
      assert len(self.Op) == len(self.opsites) 
      for self.tind,self.t in enumerate(self.ts) :
         if self.tind+1 == len(self.ts) :
            break

         realtnow = time.time()
         print('now evolving t =',self.t,flush=self.flush)

         for q in range(len(self.Hamil_Us)) :
            self.Hamil_U_i_mu=self.Hamil_Us[q]
            self.W_i_j_mu_nu=self.Hamil_Ws[q]
            self.optim_single=self.optim_singles[q]
            self.optim_double=self.optim_doubles[q]

            self.statenow=scipy.integrate.solve_ivp(
                  fun = self.__evolve_spin_strob,
                  t_span=[self.ts[self.tind],self.ts[self.tind+1]],
                  y0=self.statenow,
                  t_eval=[self.ts[self.tind+1]],
                  method=method,
                  rtol=rtol, atol=atol
                  )['y'][:,-1]

         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state=state[:,0:self.N,:]+state[:,self.N:,:]*1j
         state=state[:,self.opsites,:]

         exp_Op=np.real(np.sum(list(map(lambda x,y : x*y,  
            np.mean(state,axis=0),self.Op))
            ,axis=1))

         # This should be simplified but this is already fast
         Op_sq=np.zeros((self.numtraj,state.shape[1]),dtype=np.complex128)
         for i in range(state.shape[0]) :
            Op_sq[i,:]=np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.Op)),axis=1)**2.
         Op_sq=np.real(np.mean(Op_sq,axis=0))

         if update :
            np.savetxt(self.fout,[exp_Op],delimiter=',')
            np.savetxt(self.fvar,[Op_sq],delimiter=',')

            self.fout.close()
            self.fvar.close()

            self.fout=open(self.fname,'a')
            self.fvar=open(self.fname_var,'a')
         else :
            self.out_exps+=[exp_Op.copy()]
            self.out_exp_vars+=[Op_sq.copy()]
         
         del(state)

         elapsed = time.time() - realtnow
         print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)')

      if update :
         self.fout.close()
         self.fvar.close()
      else :
         self.out_exps=np.array(self.out_exps)
         self.out_exp_vars=np.array(self.out_exp_vars)
         np.savetxt(self.fname,self.out_exps,delimiter=',')
         np.savetxt(self.fname_var,self.out_exp_vars,delimiter=',')
      print("evolution finished")

   def __generate_optim_paths(self,y) :
      y=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
      y=y[:,:self.N,:]+y[:,self.N:,:]*1j
      self.optim_singles=[]
      self.optim_doubles=[]

      for i in range(len(self.Hamil_Us)) :
         self.optim_singles+=[oe.contract_path('in,mnk,tik -> tim',
               self.Hamil_Us[i],self.Orthomat_commute_coeffs,y,optimize='optimal')[0]]
         self.optim_doubles+=[oe.contract_path('ijsn,msk,tik,tjn ->tim',
               self.Hamil_Ws[i],self.Orthomat_commute_coeffs,y,y,optimize='optimal')[0]]

   def __evolve_spin_strob(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=self.flush)

      ynow=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim).copy()
      ynow=ynow[:,:self.N,:]+ynow[:,self.N:,:]*1j

      ydot = oe.contract('in,mnk,tik -> tim',self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,ynow,optimize=self.optim_single) \
            + \
            oe.contract('ijsn,msk,tik,tjn ->tim',self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,
                  ynow,ynow,optimize=self.optim_double)

      ydot=np.hstack((np.imag(ydot),-np.real(ydot)))
      return ydot.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)
