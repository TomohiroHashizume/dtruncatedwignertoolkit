from scipy.sparse import csr_matrix as cm

import opt_einsum as oe

import scipy.integrate
import numpy as np

import basis_mat as bm
import ortho_mats as om

import time 
import sys,os

# Python implementation of Generalized Discrete Truncated Wigner 
# for arbitrary bosons on a lattice.
# based on the following papers, please cite:
# * Schachenmayer, J., Pikovski, A., & Rey, A. M. (2015). Many-Body Quantum Spin Dynamics with Monte Carlo Trajectories on a Discrete Phase Space. 
# In Physical Review X (Vol. 5, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physrevx.5.011022.
# * Zhu, B., Rey, A. M., & Schachenmayer, J. (2019). A generalized phase space approach for solving quantum spin dynamics. 
# In New Journal of Physics (Vol. 21, Issue 8, p. 082001). IOP Publishing. https://doi.org/10.1088/1367-2630/ab354d.


global max_num_traj 
max_num_traj = 10000

def grab_choice(val,prob,numtraj) :
   return np.random.choice(val,size=(numtraj),p=prob) 

def normalize_state(state) :
   for ind in range(state.shape[0]) :
      state[ind,:] = state[ind,:]/np.sqrt(sum(state[ind,:]*np.conj(state[ind,:])))
   return state

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class gdtwa_boson :
   def __init__(self,maxn,ortho_mat_type='Gell_Mann',verbose=False) :
      self.maxn=maxn
      self.locdim=int(self.maxn+1)
      self.ortho_mat_type=ortho_mat_type
      self.verbose=verbose

      self.physicalmats=bm.bosonmat(self.maxn)
      self.b_d=self.physicalmats['b_d']
      self.b__=self.physicalmats['b__']
      self.n__=self.physicalmats['n__']
      self.n__n__mI=self.physicalmats['n__n__mI']
      self.I =self.physicalmats['I']

      if self.ortho_mat_type=='Gell_Mann' :
         self.Orthomat_mats = om.Gell_Mann_mat(self.maxn/2.)
         self.Orthomat_mats_dense = []
         for orthomat in self.Orthomat_mats :
            self.Orthomat_mats_dense.append(orthomat.todense())

      elif self.ortho_mat_type=='Clock_and_Shift' :
         print('WARNING:',ortho_mat_type,'METHOD DOES NOT WORK!')
         sys.exit(0)
         self.Orthomat_mats = om.Clock_and_Shift_mat(self.maxn/2.)
         self.Orthomat_mats_dense = []
         for orthomat in self.Orthomat_mats :
            self.Orthomat_mats_dense.append(orthomat.todense())

      else :
         print('Option',ortho_mat_type,'is not valid')
         print('Choose from: Gell_Mann [default], Clock_and_Shift')
         sys.exit(0)

      self.Orthomat_eigvecs,self.Orthomat_eigvals = om.Get_eigs(self.Orthomat_mats) 
      self.Orthomat_coeffs = om.decompose_physical_spin(self.physicalmats,self.Orthomat_mats_dense,self.ortho_mat_type)

      self.Orthomat_commute_coeffs=om.decompose_commutator_all(self.Orthomat_mats_dense,self.ortho_mat_type)
      if (len(self.Orthomat_mats) != int((self.maxn+1)**2) ) :
         print('Something went wrong upon constructing generalized Gell-Mann matricies.')
         sys.exit(0)

   def set_initial(self,state_in,numtraj,seed=1,normalize=False) :
      self.numtraj=numtraj
      if numtraj> max_num_traj :
         print('Warning: number of trajectories are greater than global lmiit ({0})'.format(max_num_traj))
         self.numtraj=max_num_traj
         print('setting numtraj to',self.numtraj)
      self.seed=seed
      np.random.seed(self.seed)
      self.physicalstate=state_in
      self.N=self.physicalstate.shape[0] 
         # Length of the chain is defined from
         # The length of the initial state

      if (state_in.shape[1] != int(self.locdim)) :
         print('input dimension does not match')
         sys.exit(0)

      if(normalize) :
         normalize_state(self.physicalstate)

      t = time.time()

      self.state0=np.array(list(map(self.sample_initstate,self.physicalstate)),dtype=np.complex128)

      self.state0=np.swapaxes(self.state0,1,2)
      self.state0=np.swapaxes(self.state0,0,1)

      print('',flush=True)
      print('shape of a state is', self.state0.shape)
      print('dtype of a state is', self.state0.dtype)

      if (self.verbose) and ( self. N <= 100) :
         print('initial state is: ')
         list(map(print , np.sum(list(map(lambda x,y : x*y,  
            np.mean(self.state0,axis=0),[self.Orthomat_coeffs['n__']]*self.N)),axis=1) ))

      elapsed = time.time() - t
      print('Finished creating intiial state: ', elapsed, ' seconds (', elapsed/60., ' minutes)')

   def sample_initstate(self,state) :
      print('i',end='',flush=True)
      rho=np.outer(np.conj(state),state)
      prob_list=list(map(self.get_probs, [rho]*(self.locdim*self.locdim),range(self.locdim*self.locdim)))
      return np.array( 
            list(map(grab_choice,self.Orthomat_eigvals,prob_list,[self.numtraj]*len(prob_list))),dtype=np.complex128
            )

   def get_probs(self,rho,GMind) :
      return np.real(list(map(lambda x: np.array(np.transpose(np.conj(x))@rho@x)[0,0],self.Orthomat_eigvecs[GMind])))

   def set_Hamil(self,h_n,J_t,U_int):
      self.h_n=h_n.copy()

      self.J_t=J_t.copy()
      self.U_int=U_int.copy()

      self.__make_Hamil()

   def __make_Hamil(self):
      t = time.time()

      self.Hamil_U_i_mu=np.zeros((self.N,self.locdim*self.locdim),dtype=np.complex128)
      for site in range(self.N) :
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['n__'])*self.h_n[site]
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['n__n__mI'])*self.U_int[site]

      J=[self.J_t,np.conj(self.J_t)]
      coeffs1=[
            self.Orthomat_coeffs['b_d'],
            self.Orthomat_coeffs['b__'],
            ]

      coeffs2=[
            self.Orthomat_coeffs['b__'],
            self.Orthomat_coeffs['b_d'],
            ]

      self.W_i_j_mu_nu=oe.contract('sij,sm,sn->ijmn',J,coeffs1,coeffs2)

      if (self.verbose) :
         print('shape of single site coefficients: ', self.Hamil_U_i_mu.shape,flush=True)

      if (self.verbose) :
         print('shape of two site coefficients: ', self.W_i_j_mu_nu.shape,flush=True)

      elapsed = time.time() - t
      print('finished creating the Hamiltonian: ', elapsed, ' seconds (', elapsed/60., ' minutes)')

      #pretty close to matlab actually, matlab is faster by 30%...


   def evolve(self,Tf,Tsteps,outdir='./',method='RK45',rtol=1e-3,atol=1e-6) :
      self.outdir=outdir
      self.t0=0 #it's fixed!
      self.Tf=Tf
      self.Tsteps=Tsteps
      self.ts=lisnpacenostart(self.Tf,self.Tsteps)
      self.statenow=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
      self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
      self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)
      self.fname=self.__generate_fname()

      self.fout=open(self.fname+'.out','w')

      self.__generate_optim_path(self.statenow.copy())

      for self.t in self.ts :
         t = time.time()
         print('now evolving t =',self.t,flush=True)
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin,
               t_span=[self.t0,self.t],
               y0=self.statenow,
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]

         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state=state[:,0:self.N,:]+state[:,self.N:,:]*1j

         exp_Op=np.real(np.sum(list(map(lambda x,y : x*y,  
            np.mean(state,axis=0),[self.Orthomat_coeffs['n__']]*self.N))
            ,axis=1))

         np.savetxt(self.fout,[exp_Op],delimiter=',',fmt='%2.15f')

         self.fout.close()
         del(state)

         self.fout=open(self.fname+'.out','a')

         self.t0=self.t
         elapsed = time.time() - t

         print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)')

      self.fout.close()

   def __generate_fname(self) :
      return self.outdir+"/out_L_{0:d}_N_{1:d}_seed_{2}".format(self.N,int(self.maxn),self.seed)

   def __generate_optim_path(self,y) :
      y=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
      y=y[:,:self.N,:]+y[:,self.N:,:]*1j
      self.optim_single=oe.contract_path('in,mnk,tik -> tim',
            self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,y,optimize='optimal')[0]
      self.optim_double=oe.contract_path('ijsn,msk,tik,tjn ->tim',
            self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,y,y,optimize='optimal')[0]

   def __evolve_spin(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=True)

      ynow=y.copy()
      ynow=ynow.reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
      ynow=ynow[:,:self.N,:]+ynow[:,self.N:,:]*1j

      ydot = oe.contract('in,mnk,tik -> tim',self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,ynow,optimize=self.optim_single) \
            + \
            oe.contract('ijsn,msk,tik,tjn ->tim',self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,
                  ynow,ynow,optimize=self.optim_double)

      ydot=np.hstack((np.imag(ydot),-np.real(ydot)))
      return ydot.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

   # Debug Functions: It doesn't mean that they are obsolete.
   def get_Orthomat_mats(self) : 
      return self.Orthomat_mats

   def get_Orthomat_coeffs(self) :
      return self.Orthomat_coeffs

   def check_Orthomat_decomposition(self) :
      return om.check_decomposition(self.physicalmats,self.Orthomat_mats,self.Orthomat_coeffs)

   def get_initial(self) :
      return self.state0
   ##########################################################
