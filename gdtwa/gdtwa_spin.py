from scipy.sparse import csr_matrix as cm

import opt_einsum as oe

import scipy.integrate
import numpy as np

import basis_mat as bm
import ortho_mats as om

import time 
import sys,os

# Python implementation of Generalized Discrete Truncated Wigner 
# for arbitrary spin magnitude, S.
# based on the following papers, please cite:
# * Schachenmayer, J., Pikovski, A., & Rey, A. M. (2015). Many-Body Quantum Spin Dynamics with Monte Carlo Trajectories on a Discrete Phase Space. 
# In Physical Review X (Vol. 5, Issue 1). American Physical Society (APS). https://doi.org/10.1103/physrevx.5.011022.
# * Zhu, B., Rey, A. M., & Schachenmayer, J. (2019). A generalized phase space approach for solving quantum spin dynamics. 
# In New Journal of Physics (Vol. 21, Issue 8, p. 082001). IOP Publishing. https://doi.org/10.1088/1367-2630/ab354d.

global max_num_traj 
max_num_traj = 10000

def grab_choice(val,prob,numtraj) :
   return np.random.choice(val,size=(numtraj),p=prob) 

def normalize_state(state) :
   for ind in range(state.shape[0]) :
      state[ind,:] = state[ind,:]/np.sqrt(sum(state[ind,:]*np.conj(state[ind,:])))
   return state

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

class gdtwa_spin :
   def __init__(self,spin,is_boson=False,ortho_mat_type='Gell_Mann',verbose=False) :
      self.spin=spin
      self.locdim=int(self.spin*2+1)
      self.is_boson=is_boson
      self.ortho_mat_type=ortho_mat_type
      self.verbose=verbose

      self.physicalmats=bm.spinmat(self.spin)
      self.Sx=self.physicalmats['Sx']
      self.Sy=self.physicalmats['Sy']
      self.Sz=self.physicalmats['Sz']
      self.Sp=self.physicalmats['Sp']
      self.Sm=self.physicalmats['Sm']
      self.I =self.physicalmats['I']

      self.physicalmats['SxSx']=self.Sx @ self.Sx
      self.physicalmats['SySy']=self.Sy @ self.Sy
      self.physicalmats['SzSz']=self.Sz @ self.Sz

      if self.ortho_mat_type=='Gell_Mann' :
         self.Orthomat_mats = om.Gell_Mann_mat(self.spin)
         self.Orthomat_mats_dense = []
         for orthomat in self.Orthomat_mats :
            self.Orthomat_mats_dense.append(orthomat.todense())

      elif self.ortho_mat_type=='Clock_and_Shift' :
         print('WARNING:',ortho_mat_type,'METHOD DOES NOT WORK!')
         sys.exit(0)
         self.Orthomat_mats = om.Clock_and_Shift_mat(self.spin)
         self.Orthomat_mats_dense = []
         for orthomat in self.Orthomat_mats :
            self.Orthomat_mats_dense.append(orthomat.todense())

      else :
         print('Option',ortho_mat_type,'is not valid')
         print('Choose from: Gell_Mann [default], Clock_and_Shift')
         sys.exit(0)

      self.Orthomat_eigvecs,self.Orthomat_eigvals = om.Get_eigs(self.Orthomat_mats) 
      self.Orthomat_coeffs = om.decompose_physical_spin(self.physicalmats,self.Orthomat_mats_dense,self.ortho_mat_type)

      self.Orthomat_commute_coeffs=om.decompose_commutator_all(self.Orthomat_mats_dense,self.ortho_mat_type)
      if (len(self.Orthomat_mats) != int((2*spin+1)**2) ) :
         print('Something went wrong upon constructing generalized Gell-Mann matricies.')
         sys.exit(0)

   def set_initial(self,state_in,numtraj,seed=1,normalize=False) :
      self.numtraj=numtraj
      if numtraj> max_num_traj :
         print('Warning: number of trajectories are greater than global lmiit ({0})'.format(max_num_traj))
         self.numtraj=max_num_traj
         print('setting numtraj to',self.numtraj)
      self.seed=seed
      np.random.seed(self.seed)
      self.physicalstate=state_in
      self.N=self.physicalstate.shape[0] 
         # Length of the chain is defined from
         # The length of the initial state

      if (state_in.shape[1] != int(self.spin*2+1)) :
         print('input dimension does not match')
         sys.exit(0)

      if(normalize) :
         normalize_state(self.physicalstate)

      t = time.time()

      self.state0=np.array(list(map(self.sample_initstate,self.physicalstate)),dtype=np.complex128)

      self.state0=np.swapaxes(self.state0,1,2)
      self.state0=np.swapaxes(self.state0,0,1)

      print('',flush=True)
      print('shape of a state is', self.state0.shape)
      print('dtype of a state is', self.state0.dtype)

      if (self.verbose) and ( self. N <= 100) :
         print('initial state is: ')
         list(map(print , np.sum(list(map(lambda x,y : x*y,  
            np.mean(self.state0,axis=0),[self.Orthomat_coeffs['Sz']]*self.N)),axis=1) + (self.is_boson)*self.spin)
            )
         print('initial variance: ')
         list(map(print , np.sum(list(map(lambda x,y : x*y*y,  
            np.var(self.state0,axis=0),[self.Orthomat_coeffs['Sz']]*self.N)),axis=1) ))

      elapsed = time.time() - t
      print('Finished creating intiial state: ', elapsed, ' seconds (', elapsed/60., ' minutes)')

   def sample_initstate(self,state) :
      if self.verbose :
         print('i',end='',flush=True)
      rho=np.outer(np.conj(state),state)
      prob_list=list(map(self.get_probs, [rho]*(self.locdim*self.locdim),range(self.locdim*self.locdim)))
      return np.array( 
            list(map(grab_choice,self.Orthomat_eigvals,prob_list,[self.numtraj]*len(prob_list))),dtype=np.complex128
            )

   def get_probs(self,rho,GMind) :
      return np.real(list(map(lambda x: np.array(np.transpose(np.conj(x))@rho@x)[0,0],self.Orthomat_eigvecs[GMind])))

   def set_Hamil(self,h_x,h_y,h_z,h_i,J_x,J_y,J_z):
      self.h_x=h_x
      self.h_y=h_y
      self.h_z=h_z
      self.h_i=h_i

      self.J_x=J_x
      self.J_y=J_y
      self.J_z=J_z

      self.__make_Hamil()

   def __make_local_Hamil(self,LogOp2,sites2):
      t = time.time()

      self.Hamil_U_i_mu_l=np.zeros((self.N,self.locdim*self.locdim),dtype=np.complex128)
      for i in range(len(sites2)) :
         self.Hamil_U_i_mu_l[sites2[i]] += np.array(LogOp2[i])

   def __make_Hamil(self):
      t = time.time()

      self.Hamil_U_i_mu=np.zeros((self.N,self.locdim*self.locdim),dtype=np.complex128)
      for site in range(self.N) :
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sx'])*self.h_x[site]
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sy'])*self.h_y[site]
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['Sz'])*self.h_z[site]
         self.Hamil_U_i_mu[site] += np.array(self.Orthomat_coeffs['I'])*self.h_i[site]

      J=[self.J_x,self.J_y,self.J_z]
      coeffs=[
            self.Orthomat_coeffs['Sx'],
            self.Orthomat_coeffs['Sy'],
            self.Orthomat_coeffs['Sz']
            ]
      self.W_i_j_mu_nu=oe.contract('sij,sm,sn->ijmn',np.array(J),np.array(coeffs),np.array(coeffs))

      if (self.verbose) :
         print('shape of single site coefficients: ', self.Hamil_U_i_mu.shape,flush=True)

      if (self.verbose) :
         print('shape of two site coefficients: ', self.W_i_j_mu_nu.shape,flush=True)

      elapsed = time.time() - t
      print('finished creating the Hamiltonian: ', elapsed, ' seconds (', elapsed/60., ' minutes)')

      #pretty close to matlab actually, matlab is faster by 30%...


   def evolve(self,Tf,Tsteps,Op=None,method='RK45',rtol=1e-3,atol=1e-6,
         fname='dat.out',update=False,flush=False,saveevery=1) :

      self.t0=0 #it's fixed!
      if Op is None :
         self.Op=[Orthomat_coeffs['Sz']]*self.N
      else :
         self.Op=Op
      self.Tf=Tf
      self.Tsteps=Tsteps

      self.ts=lisnpacenostart(self.Tf,self.Tsteps)
      self.statenow=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
      self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
      self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

      self.fname=fname
      self.fname_var=fname+'_sq'

      self.flush=flush
      if update :
         self.fout=open(self.fname,'w')
         self.fvar=open(self.fname_var,'w')

      self.out_exps=[]
      self.out_exp_vars=[]

      self.__generate_optim_path(self.statenow.copy())

      tstep=0
      for self.t in self.ts :
         begtime = time.time()

         tstep+=1

         print('now evolving t =',self.t,flush=self.flush)
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin,
               t_span=[self.t0,self.t],
               y0=self.statenow,
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]

         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state=state[:,0:self.N,:]+state[:,self.N:,:]*1j

         exp_Op=np.real(np.sum(list(map(lambda x,y : x*y,  
            np.mean(state,axis=0),self.Op))
            ,axis=1))

         # This should be simplified but this is already fast
         Op_sq=np.zeros((self.numtraj,self.N),np.complex128)
         for i in range(state.shape[0]) :
            Op_sq[i,:]=np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.Op)),axis=1)**2.
         Op_sq=np.real(np.mean(Op_sq,axis=0))

         self.out_exps+=[exp_Op.copy()]
         self.out_exp_vars+=[Op_sq.copy()]

         if update and (tstep%saveevery == 0) :
            np.savetxt(self.fout,self.out_exps,delimiter=',')
            np.savetxt(self.fvar,self.out_exp_vars,delimiter=',')

            self.fout.close()
            self.fvar.close()

            self.fout=open(self.fname,'a')
            self.fvar=open(self.fname_var,'a')

            self.out_exps.clear()
            self.out_exp_vars.clear()
            elapsed = time.time() - begtime
            print(saveevery, 'timesteps finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flush=flush)
            print('saved',flush=True)
         else :
            elapsed = time.time() - begtime
            print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flush=flush)
         
         del(state)
         self.t0=self.t

      if update :
         np.savetxt(self.fout,self.out_exps,delimiter=',')
         np.savetxt(self.fvar,self.out_exp_vars,delimiter=',')

         self.fout.close()
         self.fvar.close()

         self.out_exps = []
         self.out_exp_vars = []

      else :
         self.out_exps=np.array(self.out_exps)
         self.out_exp_vars=np.array(self.out_exp_vars)
         np.savetxt(self.fname,self.out_exps,delimiter=',')
         np.savetxt(self.fname_var,self.out_exp_vars,delimiter=',')
      print("evolution finished")

   def get_exp(self,Tf,dt,Op=None,mom=1,method='RK45',rtol=1e-3,atol=1e-6,fname='dat.out',update=False,flush=False) :
      self.t0=0 #it's fixed!
      if Op is None :
         self.Op=[Orthomat_coeffs['Sz']]*self.N
      else :
         self.Op=Op
      self.moment=mom

      self.Tf=Tf
      self.dt=dt
      self.nsw=int(self.Tf/self.dt)

      self.fname=fname
      self.fname_var=fname+'_sq'


      self.statenow=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
      self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
      self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

      self.flush=flush

      self.tindbeg=1
      if update :
         try :
            self.fout=open(self.fname,'a')
            self.fvar=open(self.fname_var,'a')
            dat=np.loadtxt(self.fname)
            dat_var=np.loadtxt(self.fname_var)
            self.t_last=dat[-1,0]
            self.t_last_var=dat_var[-1,0]
            self.tindbeg=int(self.t_last/self.dt)+1
            self.tindbeg_var=int(self.t_last_var/self.dt)+1

            assert self.t_last == self.t_last_var
            assert self.tindbeg == self.tindbeg_var
            print(f't_last={self.t_last:.3f}')
         except :
            try :
               self.fout.close()
            except :
               pass
            try :
               self.fvar.close()
            except :
               pass

            try :
               os.remove(self.fname) 
            except :
               pass
            try :
               os.remove(self.fname_var) 
            except :
               pass

            self.fout=open(self.fname,"w")
            self.fvar=open(self.fname_var,"w")
            self.tindbeg=1
      elif os.path.exists(self.fname) and os.path.exists(self.fname_var) :
         print('files') 
         print(self.fname)
         print(self.fname_var)
         print('gdtwa_spin: already exists')
         sys.exit(0)
      else :
         try :
            os.remove(self.fname) 
         except :
            pass
         try :
            os.remove(self.fname_var) 
         except :
            pass

         self.tindbeg=1

      self.out_ts=[]
      self.out_exps=[]
      self.out_exp_vars=[]

      self.__generate_optim_path(self.statenow.copy())

      for  self.tind in range(self.tindbeg,int(self.nsw+1)) :
         self.t=dt*self.tind
         physicaltnow = time.time()
         print('now evolving t =',self.t,flush=self.flush)
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin,
               t_span=[self.t0,self.t],
               y0=self.statenow,
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]

         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state=state[:,0:self.N,:]+state[:,self.N:,:]*1j

         exp_Op_arr=np.zeros((self.numtraj),dtype=np.complex128)
         for i in range(state.shape[0]) :
            exp_Op_arr[i]=np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.Op)),axis=None)**self.moment
         exp_Op = np.real(np.mean(exp_Op_arr))

         # This should be simplified but this is already fast
         Op_sq_arr=np.zeros((self.numtraj,self.N),np.complex128)
         for i in range(state.shape[0]) :
            Op_sq_arr[i,:]=(np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.Op)),axis=None)**self.moment)**2.
         Op_sq=np.real(np.mean(Op_sq_arr))

         if update :
            self.fout.write(f"{self.t:.18e} {exp_Op:.18e}\n")
            self.fvar.write(f"{self.t:.18e} {Op_sq:.18e}\n")
            if self.tind % 10 == 0 :
               self.fout.flush()
               self.fvar.flush()
         else :
            self.ts+=[self.t]
            self.out_exps+=[exp_Op]
            self.out_exp_vars+=[Op_sq]
         
         del(state)
         self.t0=self.t

         elapsed = time.time() - physicaltnow
         print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flush=True)

      if update :
         self.fout.close()
         self.fvar.close()
      else :
         self.out_exps=np.array(self.out_exps)
         self.out_exp_vars=np.array(self.out_exp_vars)
         np.savetxt(self.fname,self.out_exps,delimiter=',')
         np.savetxt(self.fname_var,self.out_exp_vars,delimiter=',')
      print("evolution finished")


   def Lecho(self,Tf,dt,initrho,
         method='RK45',rtol=1e-3,atol=1e-6,fname='dat.out',update=False,Force=False,statein=None) :

      self.t0=0 #it's fixed!

      self.initrho=initrho.copy()

      assert len(initrho) == self.N
      
      self.Tf=Tf
      self.dt=dt
      self.nsw=int(self.Tf/self.dt)
      saveevery=int(1/dt)

      self.fname=fname
      self.fname_var=fname+'_sq'
      self.fname_ts=fname+'_ts'

      if statein is None :
         self.statenow=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
         self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
         self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)
      else :
         self.statenow=statein.copy()
         self.statenow=np.hstack((np.real(self.statenow),np.imag(self.statenow)))
         self.statenow=self.statenow.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

      if os.path.exists(self.fname) and os.path.exists(self.fname_var) and (not Force):
         print('files') 
         print(self.fname)
         print(self.fname_var)
         print('gdtwa_spin: already exists')
         sys.exit(0)
      else :
         print(self.fname,os.path.exists(self.fname)) 
         print(self.fname_var,os.path.exists(self.fname_var)) 
         print(f'{Force=}') 
         try :
            os.remove(self.fname) 
         except :
            pass
         try :
            os.remove(self.fname_var) 
         except :
            pass
      self.__generate_optim_path(self.statenow.copy())

      Lechos=[]
      Lecho_sqs=[]
      ts=[]

      self.t=self.t0
      physicaltnow = time.time()
      for  tind in range(1,self.nsw+1) :
         if self.verbose :
            print('now evolving t =',self.t,flush=True)
         self.t+=dt
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin,
               t_span=[self.t0,self.t],
               y0=self.statenow,
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]


         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state=state[:,0:self.N,:]+state[:,self.N:,:]*1j

         # This should be simplified but this is already fast
         Lecho=np.zeros(self.numtraj,np.complex128)
         for i in range(state.shape[0]) :
            Lecho[i]=np.prod(np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.initrho)),axis=1))
         Lecho=np.real(np.mean(Lecho))

         Lecho_sq=np.zeros(self.numtraj,np.complex128)
         for i in range(state.shape[0]) :
            Lecho_sq[i]=np.prod(np.sum(list(map(lambda x,y : x*y,state[i,:,:],self.initrho)),axis=1))**2.
         Lecho_sq=np.real(np.mean(Lecho_sq)) # this is assured to be real

         Lechos+=[Lecho]
         Lecho_sqs+=[Lecho_sq]
         ts+=[self.t]

         if tind%saveevery==0 :
            with open(self.fname, "ab") as flecho, open(self.fname_var, "ab") as flechosq, \
                  open(self.fname_ts, "ab") as fts:
               np.savetxt(flecho,Lechos)
               Lechos.clear()
               np.savetxt(flechosq,Lecho_sqs)
               Lecho_sqs.clear()
               np.savetxt(fts,ts)
               ts.clear()
            elapsed = time.time() - physicaltnow
            print(f't={self.t:.3f}: {saveevery} steps finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flush=True)
            physicaltnow = time.time()

         del(state)
         self.t0=self.t

      with open(self.fname, "ab") as flecho, open(self.fname_var, "ab") as flechosq, \
            open(self.fname_ts, "ab") as fts:
         np.savetxt(flecho,Lechos)
         Lechos.clear()
         np.savetxt(flechosq,Lecho_sqs)
         Lecho_sqs.clear()
         np.savetxt(fts,ts)
         ts.clear()
      elapsed = time.time() - physicaltnow
      print(f't={self.t:.3f}: {saveevery} steps finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flush=True)

      print("evolution finished")

   def evaluate_otoc(self,Tf,dt,Op1=None,sites1=None,LogOp2=None,sites2=None,
         method='RK45',rtol=1e-3,atol=1e-6,fname='dat.out',update=False,flush=False) :
      self.t0=0 #it's fixed!

      if Op1 is None :
         self.Op1=[self.Orthomat_coeffs['Sz']]*self.N
      else :
         self.Op1=Op1
      if LogOp2 is None :
         self.LogOp2=[self.Orthomat_coeffs['Sz']]*self.N
      else :
         self.LogOp2=LogOp2
      
      if sites1 is None :
         self.sites1=np.arange(self.N,dtype=int)
      else :
         self.sites1=sites1
      if sites2 is None :
         self.sites2=np.arange(self.N,dtype=int)
      else :
         self.sites2=sites2

      assert len(self.Op1)==len(self.sites1)  
      assert len(self.LogOp2)==len(self.sites2)  
      assert len(self.sites1) == 1 #right now it only supports local operators
      assert len(self.sites2) == 1 #right now it only supports local operators
      self.__make_local_Hamil(self.LogOp2,self.sites2)

      self.Tf=Tf
      self.dt=dt
      self.nsw=int(self.Tf/self.dt)

      self.fname=fname
      self.fname_var=fname+'_sq'

      self.statenow0=self.state0.copy().reshape(self.numtraj,self.N*self.locdim*self.locdim)
      self.statenow0=np.hstack((np.real(self.statenow0),np.imag(self.statenow0)))
      self.statenow0=self.statenow0.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

      self.flush=flush

      self.tindbeg=1
      if update :
         try :
            self.fout=open(self.fname,'a')
            self.fvar=open(self.fname_var,'a')
            dat=np.loadtxt(self.fname)
            dat_var=np.loadtxt(self.fname_var)
            self.t_last=dat[-1,0]
            self.t_last_var=dat_var[-1,0]
            self.tindbeg=int(self.t_last/self.dt)+1
            self.tindbeg_var=int(self.t_last_var/self.dt)+1

            assert self.t_last == self.t_last_var
            assert self.tindbeg == self.tindbeg_var
            print(f't_last={self.t_last:.3f}')
         except :
            try :
               self.fout.close()
            except :
               pass
            try :
               self.fvar.close()
            except :
               pass

            try :
               os.remove(self.fname) 
            except :
               pass
            try :
               os.remove(self.fname_var) 
            except :
               pass

            self.fout=open(self.fname,"w")
            self.fvar=open(self.fname_var,"w")
            self.tindbeg=1
      elif os.path.exists(self.fname) and os.path.exists(self.fname_var) :
         print('files') 
         print(self.fname)
         print(self.fname_var)
         print('gdtwa_spin: already exists')
         sys.exit(0)
      else :
         try :
            os.remove(self.fname) 
         except :
            pass
         try :
            os.remove(self.fname_var) 
         except :
            pass

         self.tindbeg=1

      self.out_ts=[]
      self.out_otocs=[]
      self.out_otoc_vars=[]

      self.__generate_optim_path(self.statenow0.copy())

      for  self.tind in range(self.tindbeg,int(self.nsw+1)) :
         self.t=dt*self.tind 
         physical_tnow = time.time()
         print('now evaluating for t =',self.t,flush=self.flush)
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin,
               t_span=[0,self.t],
               y0=self.statenow0.copy(),
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin_local,
               t_span=[0,1.],
               y0=self.statenow,
               t_eval=[1.],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]
         self.statenow=scipy.integrate.solve_ivp(
               fun = self.__evolve_spin_rev,
               t_span=[0,self.t],
               y0=self.statenow,
               t_eval=[self.t],
               method=method,
               rtol=rtol, atol=atol
               )['y'][:,-1]

         state=self.statenow.copy().reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
         if (self.verbose) :
            print('')

         state =state[:,0:self.N,:]+state[:,self.N:,:]*1j
         state1=state[:,sites1,:].copy()

         Op1state=np.zeros((state1.shape[0],state1.shape[1]),dtype=np.complex128)
         for i in range(state1.shape[0]) :
            Op1state[i,:]=np.sum(np.array(list(map(lambda x,y : x*y,state1[i,:,:],self.Op1))),axis=1)

         otoc=np.real(np.mean(np.sum(Op1state,axis=1),axis=0))
         otoc_sq=np.real(np.mean(np.abs(np.sum(Op1state,axis=1))**2.,axis=0))

         if update :
            self.fout.write(f"{self.t:.18e} {otoc:.18e}\n")
            self.fvar.write(f"{self.t:.18e} {otoc_sq:.18e}\n")
            if self.tind % 10 == 0 :
               self.fout.flush()
               self.fvar.flush()
         else :
            self.out_ts+=[self.t]
            self.out_otocs+=[otoc]
            self.out_otoc_vars+=[otoc_sq]
        
         del(state)

         elapsed = time.time() - physical_tnow
         print('One timestep finished:', elapsed, ' seconds (', elapsed/60., ' minutes)',flash=True)

      if update :
         self.fout.close()
         self.fvar.close()
      else :
         self.out_otocs=np.array(self.out_otocs)
         self.out_otoc_vars=np.array(self.out_otoc_vars)
         np.savetxt(self.fname,np.hstack((self.out_ts,self.out_otocs)),delimiter=',')
         np.savetxt(self.fname_var,np.hastack((self.out_ts,self.out_otoc_vars)),delimiter=',')
      print("evolution finished")

   def __generate_optim_path(self,y) :
      y=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim)
      y=y[:,:self.N,:]+y[:,self.N:,:]*1j
      self.optim_single=oe.contract_path('in,mnk,tik -> tim',
            self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,y,optimize='optimal')[0]
      self.optim_double=oe.contract_path('ijsn,msk,tik,tjn ->tim',
            self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,y,y,optimize='optimal')[0]

   def __evolve_spin(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=self.flush)

      ynow=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim).copy()
      ynow=ynow[:,:self.N,:]+ynow[:,self.N:,:]*1j

      ydot = oe.contract('in,mnk,tik -> tim',self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,ynow,optimize=self.optim_single) \
            + \
            oe.contract('ijsn,msk,tik,tjn ->tim',self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,
                  ynow,ynow,optimize=self.optim_double)

      ydot=np.hstack((np.imag(ydot),-np.real(ydot)))
      return ydot.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

   def __evolve_spin_rev(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=self.flush)

      ynow=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim).copy()
      ynow=ynow[:,:self.N,:]+ynow[:,self.N:,:]*1j

      ydot = oe.contract('in,mnk,tik -> tim',-self.Hamil_U_i_mu,self.Orthomat_commute_coeffs,ynow,optimize=self.optim_single) \
            + \
            oe.contract('ijsn,msk,tik,tjn ->tim',-self.W_i_j_mu_nu,self.Orthomat_commute_coeffs,
                  ynow,ynow,optimize=self.optim_double)

      ydot=np.hstack((np.imag(ydot),-np.real(ydot)))
      return ydot.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

   def __evolve_spin_local(self,t,y) :
      if (self.verbose) :
         print('e',end='',flush=self.flush)

      ynow=y.reshape(self.numtraj,2*self.N,self.locdim*self.locdim).copy()
      ynow=ynow[:,:self.N,:]+ynow[:,self.N:,:]*1j

      ydot = oe.contract('in,mnk,tik -> tim',self.Hamil_U_i_mu_l,self.Orthomat_commute_coeffs,ynow,optimize=self.optim_single)

      ydot=np.hstack((np.imag(ydot),-np.real(ydot)))
      return ydot.reshape(self.numtraj*2*self.N*self.locdim*self.locdim)

   # Debug Functions: It doesn't mean that they are obsolete.
   def get_Orthomat_mats(self) : 
      return self.Orthomat_mats

   def get_Orthomat_coeffs(self) :
      return self.Orthomat_coeffs

   def check_Orthomat_decomposition(self) :
      return om.check_decomposition(self.physicalmats,self.Orthomat_mats,self.Orthomat_coeffs)

   def get_initial(self) :
      return self.state0
   ##########################################################
