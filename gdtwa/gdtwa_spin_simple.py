from . import basis_mat as bm
import numpy as np
import sys
from scipy import integrate

def normalize_state(state) :
   state_out=np.zeros(state.shape)
   for ind in range(state.shape[0]) :
      state_out[ind,:]=np.array(state[ind,:]/np.sqrt(sum(state[ind,:]*state[ind,:])))
   return state_out

def lisnpacenostart(Tf,steps) :
   arr,step=np.linspace(0,Tf,steps,endpoint=False,retstep=True)
   return (arr+step)

def evolve_spin(y,t,Jx,Jy,Jz,hx,hy,hz) :
   ydot=np.zeros(y.shape)
   betx=2*Jx@y[0::3]
   bety=2*Jy@y[1::3]
   betz=2*Jz@y[2::3]

   ydot[0::3]=y[2::3]*bety - y[1::3]*betz + 2*hy*y[2::3] - 2*hz*y[1::3]
   ydot[1::3]=y[0::3]*betz - y[2::3]*betx - 2*hx*y[2::3] + 2*hz*y[0::3]
   ydot[2::3]=y[1::3]*betx - y[0::3]*bety + 2*hx*y[1::3] - 2*hy*y[0::3]

   return ydot

class gdtwa_spin :
 
   def __init__(self) :
      self.spin=1/2
      self.physicalmats=bm.spinmat(self.spin)
      self.Sx=self.physicalmats['Sx']
      self.Sy=self.physicalmats['Sy']
      self.Sz=self.physicalmats['Sz']
      self.Sp=self.physicalmats['Sp']
      self.Sm=self.physicalmats['Sm']
      self.I =self.physicalmats['I']

      self.mats=[self.Sx,self.Sy,self.Sz]
      self.eigvecs=[]
      self.eigvals=[]
      for ind in range(len(self.mats)) :
         [w,v] = np.linalg.eig(self.mats[ind])
         self.eigvecs.append(v)
         self.eigvals.append(w)

   def set_initial(self,state_in,seed0=1,normalized=False) :
      self.physicalstate=state_in
      self.N=self.physicalstate.shape[0] 
         # Length of the chain is defined from
         # The length of the initial state

      if (self.physicalstate.shape[1] != int(self.spin*2+1)) :
         print('input dimension does not match')
         sys.exit(0)

      self.seed0=seed0
      np.random.seed(self.seed0)

      if(~ normalized) :
         self.physicalstate=normalize_state(self.physicalstate)
 
      self.state0=np.zeros((self.N,3)) #can be more efficient, not necessary.

      for site in range(self.N) :
         rho=np.outer(self.physicalstate[site],np.conj(self.physicalstate)[site])
         for matind in range(len(self.mats)) :
            probs=[]
            for eigind in range(int(self.spin*2+1)) :
               probs.append(
                  np.trace(
                  rho@np.outer(self.eigvecs[matind][:,eigind] ,np.conj(self.eigvecs[matind][:,eigind])
                  )))
            probs=np.real(probs)

            val=2*np.random.choice(np.real(self.eigvals[matind]),p=probs)
            self.state0[site,matind]=val

      self.state0=self.state0.reshape(self.N*3)

   def evolve(self,Tf,Tsteps,Jx,Jy,Jz,Wx=0,Wy=0,Wz=0,hx=0,hy=0,hz=0,latseed=1):
      self.t0=0 #it's fixed!
      self.Tf=Tf
      self.Tsteps=Tsteps
      self.ts = lisnpacenostart(self.Tf,self.Tsteps)

      self.latseed=latseed
      np.random.seed(latseed)

      hWx=np.random.uniform(low=-Wx,high=Wx,size=self.N)
      hWy=np.random.uniform(low=-Wy,high=Wy,size=self.N)
      hWz=np.random.uniform(low=-Wz,high=Wz,size=self.N)

      hx=hx+hWx
      hy=hy+hWy
      hz=hz+hWz

      self.y_out=integrate.odeint(func=evolve_spin , y0=self.state0, t=self.ts, args=(Jx,Jy,Jz,hx,hy,hz))
      return self.y_out
