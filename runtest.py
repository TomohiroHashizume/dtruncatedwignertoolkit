from gdtwa import gdtwa_spin_simple as gts
import numpy as np
import matplotlib.pyplot as mpl

#### PARAMS 

spin=1/2
N=100
seednum=1665982809
Tf=2
Tsteps=200


########### interaction 

############ XXZ
J_x=np.ones((N,N))
for ind in range(N) :
   J_x[ind,ind]=0

J_y=J_x
J_z=np.zeros((N,N))

############ ZZ

J_x=np.zeros((N,N))
J_y=J_x

J_z=np.ones((N,N))
for ind in range(N) :
   J_z[ind,ind]=0

h_x=np.zeros((1,N))
h_y=np.zeros((1,N))
h_z=np.zeros((1,N))

########### Initial State
state0=np.array([[1,1],]*N)


########## Auto

realizations=[]
Evolve=gts.gdtwa_spin()

for real in range(0,100) :
   trajs=[]
   for traj in range(0,100) :
      print(traj)
      Evolve.set_initial(state0,seed0=seednum+real*100+traj,normalized=False)
      y_Tf=Evolve.evolve(Tf,Tsteps,J_x,J_y,J_z,latseed=seednum+100*real)
      trajs.append(y_Tf)
   realizations.append(np.array(trajs))
