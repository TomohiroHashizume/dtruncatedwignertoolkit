# Discrete Truncated Wigner Toolkit

The software implements discrete phase space method for simulating bosonic and spin systems based on Bihui Zhu et al. (2019). 

This software is not tested yet (there isn't any publication based on this software). 

## Cite 
Please cite the following:

The software is based on:
* Bihui Zhu, Ana Maria Rey, Johannes Schachenmayer5 (2019). [New J. Phys. 21 082001](https://doi.org/10.1088/1367-2630/ab354d).

This software uses `opt_einsum`:
* Journal of Open Source Software, 2018, 3(26), 753, DOI: https://doi.org/10.21105/joss.00753

Cite this software as:
* Tomohiro Hashizume. (2024). Discrete Truncated Wigner Toolkit 0.1. [https://doi.org/10.5281/zenodo.7245728](https://doi.org/10.5281/zenodo.7245728).
Please include the date visited. 

### (non-crucial) dependency 
The test with exact diagonalization utilizes quspin. Please cite them too.
* SciPost Phys. 2, 003 (2017)

## TODO

Implement correlation measurement
and complete this read me...

## Usage

Please see the example codes in `/gdtwa/test/`

## Contact

* Tomohiro Hashizume (original developer) tomohiro.hashizume.overseas@gmail.com

## License

MIT License

Copyright (c) [2022] [Tomohiro Hashizume]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project status
Ongoing...
